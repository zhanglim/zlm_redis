package com.zkr.redis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkr.redis.service.RedisService;


/**
 * 
 * @author zhanglm
 *
 */
@RestController
public class SentinelController {

    @Autowired
    private RedisService redisService;

    private Logger logger = LoggerFactory.getLogger(SentinelController.class);


    @GetMapping("/set")
    public void setObject(String key) {
    	System.out.println("set "+key);
        redisService.set("key"+key, "value"+key);
    }
    
    @GetMapping("/setDate")
    public void setDate(String key) {
    	System.out.println("setDate "+key);
        redisService.setDate("keyDate"+key,"value Date"+key,10);
    }
    
    @GetMapping("/getDate")
    public Object getDate(String key) {
    	System.out.println("getDate "+key);
      return redisService.get("keyDate"+key);
    }

    @GetMapping("/get")
    public Object getObject(String key) {
    	System.out.println("get "+key);
      return redisService.get("key"+key);
    }
}
