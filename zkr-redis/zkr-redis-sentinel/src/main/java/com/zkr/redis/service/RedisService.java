package com.zkr.redis.service;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 
 * @author zhanglm
 *
 */
@Service
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;


    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }
    
    public void update(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }
    
    public void setDate(String key, Object value,int Time) {
        redisTemplate.opsForValue().set(key, value,Time,TimeUnit.SECONDS);
    }
}
