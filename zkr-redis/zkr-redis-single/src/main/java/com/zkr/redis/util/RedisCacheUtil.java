
package com.zkr.redis.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;


@Component
public class RedisCacheUtil {
	
	@Autowired
    private JedisPool jedisPool;
	
	private static final String KEY_SPLIT = ":"; //用于隔开缓存前缀与缓存键值
	
	
    /**
    * @Title: get
    * @Description: 根据获取key的value值，并释放连接资源
    * @param  key
    * @param     参数
    * @return String    返回类型
    */
    public String get(String key) {
        Jedis jedis = jedisPool.getResource();
        String str = "";
        try {
             str = jedis.get(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
	
    
    /**
    * @Title: set
    * @Description: 向redis中存入key和value，并释放连接资源
    * @param @param key
    * @param @param value
    * @param @return    参数
    * @return String    返回类型
    */
    public String set(String key, String value) {
        Jedis jedis = jedisPool.getResource();
        String str = "";
        try {
             str = jedis.set(key, value);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: setPre
    * @Description: 设置缓存 
    * @param @param prefix 缓存前缀（用于区分缓存，防止缓存键值重复）
    * @param @param key 缓存key
    * @param @param value 缓存value
    * @param @return    参数
    * @return
    */
    public void setPre(String prefix, String key, String value) {
        Jedis jedis = jedisPool.getResource();
        try {
            jedis.set(prefix + KEY_SPLIT + key, value);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    
    /**
    * @Title: setPreWithExpireTime
    * @Description: 设置缓存，并且自己指定过期时间
    * @param @param prefix
    * @param @param key
    * @param @param value
    * @param @param expireTime    过期时间
    * @return void    返回类型
    */
    public void setPreWithExpireTime(String prefix, String key, String value, int expireTime) {
        Jedis jedis = jedisPool.getResource();
        try {
             jedis.setex(prefix + KEY_SPLIT + key, expireTime, value);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
    * @Title: getPre
    * @Description: 获取指定key的缓存
    * @param @param prefix
    * @param @param key
    * @param @return    参数
    * @return String    返回类型
    */
    public String getPre(String prefix, String key) {
        Jedis jedis = jedisPool.getResource();
        String str = "";
        try {
        	 str =jedis.get(prefix + KEY_SPLIT + key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: deleteWithPrefix
    * @Description: 删除指定key的缓存
    * @param @param prefix
    * @param @param key    参数
    * @return void    返回类型
    */
    public void deleteWithPrefix(String prefix, String key) {
        Jedis jedis = jedisPool.getResource();
        try {
        	 jedis.del(prefix + KEY_SPLIT + key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    
    /**
    * @Title: delete
    * @Description: 删除指定key的缓存
    * @param @param key    参数
    * @return void    返回类型
    */
    public void delete(String key) {
        Jedis jedis = jedisPool.getResource();
        try {
        	 jedis.del(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    
    
    /**
    * @Title: expire
    * @Description: 设置存存活时间
    * @param @param key
    * @param @param second
    * @param @return    参数
    * @return Long    返回类型
    */
    public Long expire(String key, int second) {
        Jedis jedis = jedisPool.getResource();
        Long str = null;
        try {
        	 str = jedis.expire(key,second);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: ttl
    * @Description: 判断key多久过期
    * @param @param key
    * @param @return    参数
    * @return Long    返回类型 秒
    * >= 0     剩余秒数
    * = -1    永久存活
    * = -2    已经消除
    */
    public Long ttl(String key) {
        Jedis jedis = jedisPool.getResource();
        Long str = null;
        try {
        	 str = jedis.ttl(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: setWithExpireTime
    * @Description: 设置缓存，并且自己指定过期时间
    * @param @param key
    * @param @param value
    * @param @param expireTime    过期时间
    * @return void    返回类型
    */
    public void setWithExpireTime(String key, String value, int expireTime) {
        Jedis jedis = jedisPool.getResource();
        try {
        	 jedis.setex(key, expireTime, value);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
    * @Title: exists
    * @Description: 判断缓存中是否有对应的value
    * @param @param key
    * @param @return    参数
    * @return boolean    返回类型
    */
    public boolean exists(final String key) {
        Jedis jedis = jedisPool.getResource();
        boolean str = false;
        try {
        	str = jedis.exists(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: decr
    * @Description: 对一个key的值 --
    * @param @param key
    * @param @return    参数
    * @return long    返回类型
    */
    public long decr(String key) {
        Jedis jedis = jedisPool.getResource();
        Long str = null;
        try {
        	str = jedis.decr(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    /**
    * @Title: incr
    * @Description: 对一个key的值进行++，并释放连接资源
    * @param @param key
    * @param @return    参数
    * @return long    返回类型
    */
    public long incr(String key) {
        Jedis jedis = jedisPool.getResource();
        Long str = null;
        try {
             str = jedis.incr(key);
        } finally {
            try {
                jedis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    /**
     * @Title: incr
     * @Description: 对一个key的值进行++，并释放连接资源
     * @param @param key
     * @param @return    参数
     * @return long    返回类型
     */
    public long incrPre(String prefix,String key){
        return incr(prefix + KEY_SPLIT + key);
    }
    
    
    
    
    
    
    
    
    
    
	
	
	
	
}
