package com.zkr.redis.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


@Configuration
@EnableCaching
public class RedisCacheConfiguration extends CachingConfigurerSupport{
	Logger logger = LoggerFactory.getLogger(RedisCacheConfiguration.class);
	
	//Redis服务器IP
    @Value("${redis.cache.addr}")
    private String addr;
    
    //Redis的端口号
    @Value("${redis.cache.port}")
    private int port;
    
    //Redis的密码
    @Value("${redis.cache.password}")
    private String password;
    
    //可用连接实例的最大数目，默认值为8；
    //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    @Value("${redis.cache.maxActive}")
    private int maxActive;
    
    //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值8。
    @Value("${redis.cache.maxIdle}")
    private int maxIdle;
    
    //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    @Value("${redis.cache.maxWait}")
    private int maxWait;
    
    @Value("${redis.cache.timeout}")
    private int timeout;
    
    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    @Value("${redis.cache.testOnBorrow}")
    private boolean testOnBorrow;
    
    
    @Bean
    public JedisPool redisPoolFactory() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxWaitMillis(maxWait);
        jedisPoolConfig.setMaxTotal(maxActive);
        jedisPoolConfig.setTestOnBorrow(testOnBorrow);
        JedisPool jedisPool = null;
        System.out.println("addr:"+addr+",port:"+port+",password:"+password);
        if(StringUtil.isEmpty(password)){
        	jedisPool = new JedisPool(jedisPoolConfig,addr,port,timeout);
        }else{
        	jedisPool = new JedisPool(jedisPoolConfig,addr,port,timeout,password);
        }
        logger.info("JedisPool注入成功！！");
        logger.info("redis地址：" + addr + ":" + port);
        return jedisPool;
    }
	
	
	
}
