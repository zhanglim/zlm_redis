package com.zkr.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class RedisSingleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisSingleApplication.class, args);
    }
}