/**
 * @Copyright ®2017 Sinosoft Co. Ltd. All rights reserved.<br/>
 * 项目名称 : 交银康联智能运营平台
 * 创建日期 : 2017年5月24日
 * 修改历史 : 
 *     1. [2017年5月24日]创建文件 by sinosoft
 */
package com.zkr.redis.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 
* @ClassName: RedisProperties
* @Description: redis配置文件
* @author zhanglm
* @date 2017年12月14日
*
 */
@Configuration
public class RedisProperties {
    @Value("${redis.cache.expireSeconds}")
    private int expireSeconds;
    @Value("${redis.cache.clusterNodes}")
    private String clusterNodes;
    @Value("${redis.cache.commandTimeout}")
    private int commandTimeout;
    
    @Value("${redis.cache.redisPwd}")
    private String redisPwd;
    
    

    public String getRedisPwd() {
		return redisPwd;
	}

	public void setRedisPwd(String redisPwd) {
		this.redisPwd = redisPwd;
	}

	public String getClusterNodes() {
        return clusterNodes;
    }

    public void setClusterNodes(String clusterNodes) {
        this.clusterNodes = clusterNodes;
    }

    public int getCommandTimeout() {
        return commandTimeout;
    }

    public void setCommandTimeout(int commandTimeout) {
        this.commandTimeout = commandTimeout;
    }

    public int getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(int expireSeconds) {
        this.expireSeconds = expireSeconds;
    }

}