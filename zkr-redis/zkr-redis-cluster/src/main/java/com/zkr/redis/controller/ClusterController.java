package com.zkr.redis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkr.redis.util.RedisClusterUtil;




/**
 * 
 * @author zhanglm
 *
 */
@RestController
public class ClusterController {

    @Autowired
    private RedisClusterUtil redisService;

    private Logger logger = LoggerFactory.getLogger(ClusterController.class);


    @GetMapping("/set")
    public void setObject(String key) {
    	System.out.println("set "+key);
        redisService.set("key"+key, "value"+key);
    }
    
    

    @GetMapping("/get")
    public Object getObject(String key) {
    	System.out.println("get "+key);
      return redisService.get("key"+key);
    }
}
